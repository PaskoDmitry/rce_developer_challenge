# Magento 2 Module RCE DeveloperChallenge

    ``rce/module-developerchallenge``

 - [Main Functionalities](#markdown-header-main-functionalities)
 - [Installation](#markdown-header-installation)
 - [Specifications](#markdown-header-specifications)


## Main Functionalities
A simple extension of magento 2 for magento 2 api, which is a simple calculator

Use this endpoint for testing /V1/api/rce/calculator
The body of the request will be as follows:
{
"left": 12.34,
"right": 56.78,
"operator": "string",
"precision": 2
}

Where operator will be one of the following:

 - add
 - subtract
 - multiply
 - divide
 - power
 - precision is optional
 
Example of request: 
curl -d '{"left": 12.34, "right": 56.78, "operator": "add", "precision": 2 }' -H 'Content-Type: application/json' http://localhost/index.php/rest/V1/api/rce/calculator


## Installation

### Type 1: Zip file
 - Unzip the zip file in `app/code/RCE/DeveloperChallenge`
 - Enable the module by running `php bin/magento module:enable RCE_DeveloperChallenge`
 - Apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`

### Type 1: Using git clone
 - git clone https://PaskoDmitry@bitbucket.org/PaskoDmitry/rce_developer_challenge.git . in `app/code/RCE/DeveloperChallenge`
 - Enable the module by running `php bin/magento module:enable RCE_DeveloperChallenge`
 - Apply database updates by running `php bin/magento setup:upgrade`\*
 - Flush the cache by running `php bin/magento cache:flush`
 
## Specifications

 - API Endpoint
	- POST - RCE\DeveloperChallenge\Api\CalculatorManagementInterface > RCE\DeveloperChallenge\Model\CalculatorManagement



