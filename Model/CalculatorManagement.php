<?php


namespace RCE\DeveloperChallenge\Model;

class CalculatorManagement implements \RCE\DeveloperChallenge\Api\CalculatorManagementInterface
{

    private $logger;

    public function __construct(\Psr\Log\LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * {@inheritdoc}
     */
    public function postCalculator($left, $right, $operator, $precision = 2)
    {
        try {
            switch($operator)
            {
                case "add";
                    $compute = round($left + $right, $precision);
                    return json_encode([
                        'status' => 'OK',
                        'result' => $compute
                    ]);

                    break;
                case "subtract";
                    $compute = round($left - $right, $precision);
                    return json_encode([
                        'status' => 'OK',
                        'result' => $compute
                    ]);
                    break;
                case "multiply";
                    $compute = round($left * $right, $precision);
                    return json_encode([
                        'status' => 'OK',
                        'result' => $compute
                    ]);
                    break;
                case "divide";
                    $compute = round($left / $right, $precision);
                    return json_encode([
                        'status' => 'OK',
                        'result' => $compute
                    ]);
                    break;
                case "power";
                    $compute = round(pow($left, $right), $precision);
                    return json_encode([
                        'status' => 'OK',
                        'result' => $compute
                    ]);
                    break;

                default:
                    return json_encode([
                        'status' => 'OK',
                        'result' => 'Please specify "operator"'
                    ]);
            }
        } catch (\Exception $e) {
            return json_encode([
                'status' => 'Error',
                'result' => $e->getMessage()
            ]);
        }
    }
}
