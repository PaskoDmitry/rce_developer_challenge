<?php


namespace RCE\DeveloperChallenge\Api;

interface CalculatorManagementInterface
{

    /**
     * POST for calculator api
     * @param string $left
     * @param string $right
     * @param string $operator
     * @param string $precision
     * @return string
     */
    public function postCalculator($left, $right, $operator, $precision = 2);
}
